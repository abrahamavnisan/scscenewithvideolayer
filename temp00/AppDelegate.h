//
//  AppDelegate.h
//  temp00
//
//  Created by Abraham Avnisan on 6/14/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

