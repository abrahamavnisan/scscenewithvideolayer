# README #

I'm attempting to assign an AVCaptureVideoPreviewLayer to the background contents of an SCNScene. The documentation [clearly states that this is possible][1], and while I am able to get the video preview to work in a UIView, I am unable to get it working in my SCNScene.

Setting the `useCaptureView` BOOL to `YES` demonstrates that the video preview code works in a UIView.

Any help would be very much appreciated!

  [1]: https://developer.apple.com/library/ios/documentation/SceneKit/Reference/SCNMaterialProperty_Class/index.html#//apple_ref/occ/instp/SCNMaterialProperty/contents