//
//  ViewController.m
//  temp00
//
//  Created by Abraham Avnisan on 6/14/16.
//  Copyright © 2016 Abraham Avnisan. All rights reserved.
//

#import "ViewController.h"

@import SceneKit;
@import AVFoundation;

@interface ViewController ()

@property (strong, nonatomic) SCNView *sceneView;

// AV Foundation Properties
@property (strong, nonatomic) AVCaptureSession *captureSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *captureLayer;
@property (strong, nonatomic) UIView *captureView;

@end

@implementation ViewController

- (void)setup
{
    
    // create an SCNView and add it to the view
    self.sceneView = [[SCNView alloc] initWithFrame:self.view.bounds options:nil];
    SCNScene *scene = [[SCNScene alloc] init];
    self.sceneView.scene = scene;
    [self.view addSubview:self.sceneView];
    
    // create a smaller UIView to test capture video layer
    float height = self.view.bounds.size.height / 2.0;
    float width = self.view.bounds.size.width / 2.0;
    CGRect frame = CGRectMake(self.view.bounds.size.width / 2.0 - width / 2.0, self.view.bounds.size.height / 2.0 - height / 2.0, width, height);
    self.captureView = [[UIView alloc] initWithFrame:frame];
    [self.view addSubview:self.captureView];
    
    // set up and start video preview
    [self startCamera];
}
- (void)startCamera
{
    AVCaptureDevice *camera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!camera) {
        NSLog(@"ERROR - could not access camera");
        return;
    }
    self.captureSession = [[AVCaptureSession alloc] init];
    AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:camera error:nil];
    [self.captureSession addInput:newVideoInput];
    
    self.captureLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    [self.captureLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.captureSession startRunning];
    });
    
    BOOL useCaptureView = NO;
    
    if (useCaptureView) {
        
        self.captureLayer.frame = self.captureView.bounds;
        [self.captureView.layer addSublayer:self.captureLayer];
        self.sceneView.backgroundColor = [UIColor blueColor];
        
    } else {
        
        self.captureLayer.frame = self.sceneView.bounds;
        self.sceneView.scene.background.contents = self.captureLayer;
        self.captureView.backgroundColor = [UIColor redColor];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
